enum FileTypes {
    posts = 'posts',
    pages = 'pages',
    drafts = 'drafts',
}

type BasicFile = {
    filename: string,
    id: number,
}

type Post = BasicFile & {
    title: string,
    date: string,
    tags: [string],
}

type Page = BasicFile & {
    title: string,
    date: string,
}

type Draft = BasicFile & {
    title: string,
    tags: [string],
}

const FileDirectories: Record<FileTypes, string> = {
    [FileTypes.posts]: '/source/_posts',
    [FileTypes.pages]: '/source',
    [FileTypes.drafts]: '/source/_drafts'
}

const FileNames: Record<FileTypes, string> = {
    [FileTypes.posts]: 'Post',
    [FileTypes.pages]: 'Page',
    [FileTypes.drafts]: 'Draft'
}

type AnyFile = Page | Draft | Post;

export {
    FileDirectories,
    FileNames,
    Post,
    Page,
    Draft,
    AnyFile,
    BasicFile,
    FileTypes,
}