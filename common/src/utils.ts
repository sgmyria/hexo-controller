const noOp: () => void = () => {
    // Function intentionally left blank
}

const genId = (() =>
  // Good luck!
  // https://stackoverflow.com/a/37438675/1798148
  (m = Math, d = Date, h = 16, s = (x: number) => m.floor(x).toString(h)) => // eslint-disable-line
  s(d.now() / 1000) + ' '.repeat(h).replace(/./g, () => s(m.random() * h)))()

export {
    noOp,
    genId,
}