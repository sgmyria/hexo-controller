import Dexie from 'dexie';
import { makeRequest } from './network';
import { Post, Page, Draft, FileTypes, FileDirectories } from 'hexapod-common';


class LocalHexapodDatabase extends Dexie {
    posts!: Dexie.Table<Post, number>;
    pages!: Dexie.Table<Page, number>;
    drafts!: Dexie.Table<Draft, number>;

    constructor() {
        super('HexapodDb');
        this.version(1).stores({
            [FileTypes.posts]: '++id, filename, title, date',
            [FileTypes.pages]: '++id, filename, title, date',
            [FileTypes.drafts]: '++id, filename, title',
        });
    }
}

const db = new LocalHexapodDatabase();

const compareWithRemote = async (filetype: FileTypes) => {
    try {
        const remotes = await makeRequest(`/writing/listAll/${filetype}`);
        const localsRaw = await db[filetype].toArray();
        const locals = localsRaw.map((file) => file.filename)
        const requiredRemotes = [];
        for (const remote of remotes.files) {
            if (!locals.includes(remote)) {
                requiredRemotes.push(remote)
            }
        }
        const removals = [];
        for (const local of localsRaw) {
            if (!remotes.files.includes(local.filename)) {
                removals.push(local.id);
            }
        }
        const cleanedRemotes = requiredRemotes.filter(remote => remote[0] !== '_');
        if (cleanedRemotes.length) {
            try {
                const { details } = await makeRequest(`/writing/fileDetails/${filetype}`, { files: cleanedRemotes });
                for (const filename of Object.keys(details)) {
                    const file = {
                        filename,
                        ...details[filename]
                    }
                    await db[filetype].put(file);
                }
            } catch(e) {
                throw new Error(`Failed to fetch individual remote for type ${filetype} due to error: ${e}`)
            }
        }
        if (removals.length) {
            for (const filename of removals) {
                await db[filetype].delete(filename);
            }
        }
    } catch(e) {
        console.error(e)
        throw new Error(`Failed to fetch remote list for type ${filetype}`)
    }

}

export {
    db,
    compareWithRemote,
}