// const remote = process.env.APP_ENV === 'production' ? 'https://caterjunes.com/hc' : 'http://localhost:3001/hc';
const remote = 'https://caterjunes.com/hc';

type TMakeRequest = (endpoint: string, jsonBody?: any) => any;
type TOptions = {
    method: string,
    headers: Record<string, string>,
    body?: any
}

const options: TOptions = { 
    method: 'POST', 
    headers: {
        'Content-Type': 'application/json',
        'token': 'colour-sound-oblivion'
    },
}

const makeRequest: TMakeRequest = async (endpoint, jsonBody) => {
    const opts = {...options};
    if (jsonBody) {
        opts.body = JSON.stringify(jsonBody);
    }
    const url = `${remote}${endpoint}`;
    const resp = await fetch(url, opts)
    const json = await resp.json();
    return json;
}

const performUpload: TMakeRequest = async (endpoint, file) => {
    const formData = new FormData();
    formData.append('file', file);
    const url = `${remote}${endpoint}`;
    const resp = await fetch(url, {
        method: 'POST',
        headers: {
            'token': 'colour-sound-oblivion',
        },
        body: formData
    })
    const json = await resp.json();
    return json;
}

export {
    makeRequest,
    performUpload,
};