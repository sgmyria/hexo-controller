import { Tab, Tabs } from 'react-bootstrap';
import { useState } from 'react';
import { TocInstance } from './tocInstance';
import { useAppStateContext } from 'context/appState';
import { FileTypes, FileNames } from 'hexapod-common';

const TableOfContents = () => {
    const { document, setDocument } = useAppStateContext();
    const [selectedTab, setSelectedTab] = useState<FileTypes>(document.type || FileTypes.posts);
    const onSelect = (nextTab: string | null) => {
        if (!nextTab) return;
        const nextType = FileTypes[nextTab as FileTypes];
        setSelectedTab(nextType);
        setDocument({type: nextType, filename: null})
    }
    return (
        <Tabs
            activeKey={selectedTab}
            onSelect={onSelect}
        >
            {Object.keys(FileTypes).map((curType) => (
                <Tab key={curType} eventKey={curType} title={FileNames[curType as FileTypes]}>
                    <TocInstance fileType={curType as FileTypes} />
                </Tab>
            ))}
        </Tabs>
    )
}

export { TableOfContents };