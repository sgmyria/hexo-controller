import styled from 'styled-components';
import { Table } from 'react-bootstrap';
import { useFileList } from './hooks/useFileList';
import { useAppStateContext, DocumentInfo } from '../../context/appState';
import { FileTypes, BasicFile, Page, Post } from 'hexapod-common';
import { FC } from 'react';

const TableContainer = styled.div`

`;

const renderHead = (type: FileTypes) => {
    switch (type) {
        case FileTypes.pages:
        case FileTypes.posts:
            return (
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Date</th>
                    </tr>
                </thead>
            )
        case FileTypes.drafts:
            return (
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Title</th>
                    </tr>
                </thead>
            )
    }
}

const renderFile = (setDocument: (doc: DocumentInfo) => void, type: FileTypes) => (file: BasicFile) => {
    switch (type) {
        case FileTypes.pages:
        case FileTypes.posts:
            const { id, title, date, filename } = file as Post;
            return (
                <tr key={`${filename}`} onClick={() => {
                    setDocument({
                        type, filename
                    })
                }}>
                    <td>{id}</td>
                    <td>{title}</td>
                    <td>{date}</td>
                </tr>
            )
        case FileTypes.drafts:
            return (
                <tr key={`${file.filename}`} onClick={() => {
                    setDocument({
                        type, filename: file.filename
                    })
                }}>
                    <td>{file.id}</td>
                    <td>{(file as Page).title}</td>
                </tr>
            );
    }

}

type TocInstanceProps = {
    fileType: FileTypes,
}

const TocInstance: FC<TocInstanceProps> = ({ fileType }) => {
    const { fileList } = useFileList(fileType);
    const { setDocument } = useAppStateContext();
    return (
        <TableContainer>
            <Table striped bordered hover>
                {renderHead(fileType)}
                <tbody>
                    {fileList.map(renderFile(setDocument, fileType))}
                </tbody>
            </Table>
        </TableContainer>
    );
};

export { TocInstance };