import styled from 'styled-components';
import { useAppStateContext } from '../../context/appState';
import { TableOfContents } from './toc';
import { EditControls } from './editControls';

const TocContainer = styled.div`
    border: 1px solid cornflowerblue;
    margin: 20px;
    padding: 20px;
    border-radius: 20px;
    flex: 1;
`;

const renderPaneContent = (filename: string | null) => {
    if (!filename) {
        return (<TableOfContents />);
    } else {
        return (<EditControls />);
    }
}

const LeftPane = () => {
    const { document } = useAppStateContext();
    return (
        <TocContainer>
            {renderPaneContent(document.filename)}
        </TocContainer>
    );
};

export { LeftPane };