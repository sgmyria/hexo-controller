import { Button } from 'react-bootstrap';
import { useAppStateContext } from '../../context/appState';
import { useFullFile } from '../editorArea/hooks/useFullFile';
import { getEditorValue } from '../editorArea';
import { useState } from 'react';
import styled from 'styled-components';
import { FileTypes } from 'hexapod-common/dist';

const ButtonContainer = styled.div`
    display: flex;
    align-content: space-around;
    flex-direction: column;
    height: 100%;
`;

const TopContainer = styled.div`
    display: flex;
`;

const BottomContainer = styled.div`
    display: flex;
    align-self: flex-end;
    align-content: flex-end;
    align-items: flex-end;
    flex: 1;
`;

const StyledButton = styled(Button)`
    flex: 1;
    margin: 2px;
    max-height: 38px;
`;

const EditControls = () => {
    const [loading, setLoading] = useState(false);
    const { document, setDocument } = useAppStateContext();
    const { saveFile, deleteFile } = useFullFile();
    const cancel = () => {
        setDocument({ type: document.type, filename: null });
    }
    const clickSave = async () => {
        setLoading(true);
        const editorValue = getEditorValue();
        await saveFile(editorValue || '');
        setLoading(false);
    }
    const clickDelete = async () => {
        setLoading(true);
        await deleteFile();
        setLoading(false);
        setDocument({ type: document.type, filename: null });
    }

    const renderDraft = () => {
        if (document.type === FileTypes.drafts) {
            return (
                <TopContainer>
                    <StyledButton disabled={loading} onClick={cancel}>Publish draft</StyledButton>
                </TopContainer>
            );
        } 
        return (<></>);
    }
    return (
        <ButtonContainer>
            <TopContainer>
                <StyledButton disabled={loading} onClick={clickSave}>Save</StyledButton>
                <StyledButton disabled={loading} onClick={cancel}>Cancel</StyledButton>
            </TopContainer>
            {renderDraft()}
            <BottomContainer>
                <StyledButton disabled={loading} onClick={clickDelete} variant="danger">Delete</StyledButton>
            </BottomContainer>
        </ButtonContainer>
    )
}

export { EditControls };