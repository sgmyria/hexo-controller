import { useState, useEffect } from 'react';
import { db } from '../../../scripts/db';
import { FileTypes, AnyFile } from 'hexapod-common';



const useFileList = (fileType: FileTypes) => {
    const [fileList, setFileList] = useState<AnyFile[]>([]);
    useEffect(() => {
        db[fileType].toArray()
            .then(setFileList);
    });
    return {
        fileList
    }
}

export { useFileList }