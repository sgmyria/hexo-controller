import styled from 'styled-components';
import { FC } from 'react';
import { useWindowSize } from 'hooks/useWindowSize';

const OuterWrapper = styled.div`
    padding: 3px;
    background-color: cornflowerblue;
    height: 100%;
`
const InnerWrapper = styled.div`
    display: flex;
    flex-flow: column;
    border-radius: 20px;
    padding: 10px;
    margin: 10px;
    background-color: white;
`;

const ContentHolder = styled.div`
    flex: 1 1 auto;
    display: flex;
`;

type BasicWrapper = {
    children: React.ReactNode,
}
const ContentWrapper: FC<BasicWrapper> = ({children}) => {
    return (
        <ContentHolder>
            {children}
        </ContentHolder>
    );
}

const Wrapper: FC<BasicWrapper> = ({children}) => {
    const { height } = useWindowSize();
    return (
        <OuterWrapper>
            <InnerWrapper style={{minHeight: `${height - 30}px`}}>
                {children}
            </InnerWrapper>
        </OuterWrapper>
    );
};

export { Wrapper, ContentWrapper };