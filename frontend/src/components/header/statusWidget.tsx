import { FC } from 'react';
import styled from 'styled-components';
import { CloudLightning, CloudOff, UploadCloud } from 'react-feather';
import { useHexoStatus, HexoStatuses } from './hooks/useHexoStatus';

const WidgetContainer = styled.div`
    margin-top: auto;
    margin-bottom: auto;
    margin-right: 40px;
    padding: 20px;
    width: 200px;
`;

const OnlineIcon = styled(CloudLightning)`
    stroke: green;
`;

const OfflineIcon = styled(CloudOff)`
    stroke: red;
`;

const LoadingIcon = styled(UploadCloud)`
    stroke: gold;
`;

const Label = styled.div`
    margin-left: 10px;
    display: inline-block;
`

const ToggleLink = styled.div`
    color: cornflowerblue;
    text-decoration: underline;
    margin-left: 35px;
`

const StatusContainer = styled.div`

`

const renderStatus = (status: HexoStatuses) => {
    if (status === HexoStatuses.Online) {
        return (
            <StatusContainer>
                <OnlineIcon />
                <Label>Hexo Online</Label>
            </StatusContainer>
        );
    } else if(status === HexoStatuses.Loading) {
        return (
            <StatusContainer>
                <LoadingIcon />
                <Label>Hexo Updating</Label>
            </StatusContainer>
        )
    } else {
        return (
            <StatusContainer>
                <OfflineIcon />
                <Label>Hexo Offline</Label>
            </StatusContainer>
        )
    }
}

const StatusWidget: FC = () => {
    const { status, generate, test } = useHexoStatus();

    return (
        <WidgetContainer>
            {renderStatus(status)}
            <ToggleLink onClick={generate}>Generate</ToggleLink>
            <ToggleLink onClick={test}>Test</ToggleLink>
        </WidgetContainer>
    )
}

export {
    StatusWidget
}