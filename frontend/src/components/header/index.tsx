import styled from 'styled-components';
import { StatusWidget } from './statusWidget';

const HeaderContainer = styled.div`
    margin: 20px;
    padding: 20px;
    border-radius: 20px;
    border: 1px solid cornflowerblue;
    flex: 0 1 auto;
    display: flex;
`;

const Title = styled.h1`
    flex: 1
`;

const Header = () => {
    return (
        <HeaderContainer>
            <Title>Hexopod</Title>
            <StatusWidget />
        </HeaderContainer>
    );
};

export { Header };