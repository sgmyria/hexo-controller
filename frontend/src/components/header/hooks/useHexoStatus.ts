import { useState, useEffect } from 'react';
import { makeRequest } from '../../../scripts/network';

enum HexoStatuses {
    Online = 'online',
    Loading = 'loading',
    Offline = 'offline'
}

const STATUS_REFRESH_SPEED = 1000 * 60;

const checkServerStatus: () => Promise<HexoStatuses> = async () => {
    try {
        const res = await makeRequest(`/control/status`);
        if (res.ok) {
            if (res.running) {
                return HexoStatuses.Online;
            } else {
                return HexoStatuses.Offline;
            }
        } else {
            console.warn('Problem fetching status', res.error);
            return HexoStatuses.Offline;
        }
    } catch(e) {
        console.warn('Error fetching status', e)
        return HexoStatuses.Offline;
    }
}

const useHexoStatus = () => {
    const [ status, setStatus ] = useState<HexoStatuses>(HexoStatuses.Offline);
    const [ randomString, setRandomString ] = useState<string>('1');
    
    const refreshStatus = () => {
        setRandomString(`${Math.random()}`)
    }

    useEffect(() => {
        let thisTimeout = setTimeout(() => {});
        const checkStatus = async () => {
            const currentStatus = await checkServerStatus();
            setStatus(currentStatus);
            thisTimeout = setTimeout(async () => {
                refreshStatus();
            }, STATUS_REFRESH_SPEED)
        }

        checkStatus();
        return () => {
            clearTimeout(thisTimeout);
        }

    }, [randomString]);

    useEffect(() => {
        refreshStatus();
    }, [])

    const generate = () => {
        return new Promise<void>(async (resolve, reject) => {
            setStatus(HexoStatuses.Loading);
            const res = await makeRequest(`/control/generate`);
            if (res.error) {
                throw new Error(res.error);
            }
            refreshStatus();
            resolve();
        });
    }

    const test = () => {
        return new Promise<void>(async (resolve, reject) => {
            const res = await makeRequest(`/control/test`);
            if (res.error) {
                throw new Error(res.error);
            }
            console.log(res);
            resolve();
        });
    }

    return {
        status,
        test,
        generate
    }
}

export { useHexoStatus, HexoStatuses }