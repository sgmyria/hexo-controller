import styled from 'styled-components';
import { useAppStateContext, AppStates } from '../context/appState';
import { compareWithRemote } from '../scripts/db';
import { useEffect } from 'react';
import { FileTypes } from 'hexapod-common';

const PreloaderWrapper = styled.div`
    width: 98%;
    height: 98%;
    margin: auto;
`;

const Title = styled.h1`
    text-align: center;
`;

const Preloader = () => {
    const { setAppState } = useAppStateContext();
    useEffect(() => {
        const checkBackend = async () => {
            for( const type of Object.keys(FileTypes)){
                await compareWithRemote(type as FileTypes);
            }
            setAppState(AppStates.mainScreen)
        }
        checkBackend();
    }, []);
    return (
        <PreloaderWrapper>
            <Title>Loading</Title>
        </PreloaderWrapper>
    );
};

export { Preloader };