import { useState, useEffect } from 'react';
import { db } from '../../../scripts/db';
import { makeRequest } from '../../../scripts/network';
import { FileTypes } from 'hexapod-common';
import { useAppStateContext } from 'context/appState';

const newFile = async (type: FileTypes, name: string) => {
    const {fileDetails, filename} = await makeRequest(`/writing/new/${type}`, {name});
    await db[type].put({...fileDetails, filename});
    return Promise.resolve(filename);
}

const useFullFile = () => {
    const [file, setFile] = useState<null | any>(null);
    const { document } = useAppStateContext();
    const { type, filename } = document
    useEffect(() => {
        setFile(null);
        if (type && filename) {
            db[type].get({filename})
                .then(async (file) => {
                    const {fileContent} = await makeRequest(`/writing/fileContent/${type}`, {file: filename})
                    setFile({
                        ...file,
                        content: fileContent
                    });
                });
        } else {
            setFile(null);
        }
    }, [type, filename]);

    const saveFile = async (content: string) => {
        if (type && filename && content) {
            const details = await db[type].get({filename});
            const res = await makeRequest(`/writing/update/${type}`, {name: filename, details, content});
            if (res.ok) {
                return Promise.resolve()
            }
        }
        return Promise.reject('Error saving file')
    }

    const deleteFile = async () => {
        if (type) {
            const details = await db[type].get({filename});
            if (details) {
                await makeRequest(`/writing/delete/${type}`, {name: filename});
                await db[type].delete(details.id);
            }
        }
    }

    return {
        file,
        saveFile,
        deleteFile
    }
}

export { useFullFile, newFile }