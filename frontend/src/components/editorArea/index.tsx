import styled from 'styled-components';
import { useFullFile } from './hooks/useFullFile';
import Editor from 'rich-markdown-editor';
import { NewFileModal } from '../newFileModal';
import { performUpload } from 'scripts/network';

const EditorContainer = styled.div`
    flex: 5;
    margin: 20px;
    border-radius: 20px;
    border: 1px solid cornflowerblue;
    padding: 20px;
    padding-left: 30px;
`;

const TitleContainer = styled.div`
    height: 41px;
    padding-bottom: 10px;
`

const EditorHolder = styled.div`
    height: 100%;
    border-top: 1px solid #dee2e6;
    padding-top: 20px;
`;

const Title = styled.h2`

`

let persistentValueGetter: () => string | null = () => null;

const getEditorValue = () => {
    const val = persistentValueGetter();
    if (val) {
        return val.replace(/^\\/, "");
    }
    return null;
}

const uploadImage = async (file: Blob) => {
    const { location } = await performUpload('/content/upload', file)
    return Promise.resolve(location);
}

const EditorArea = () => {
    const { file } = useFullFile();

    if (!file) {
        return (
            <EditorContainer>
                <NewFileModal />
            </EditorContainer>
        );
    }
    return (
        <EditorContainer>
            <TitleContainer>
                <Title>
                    {file.title}
                </Title>
            </TitleContainer>
            <EditorHolder>
                <Editor 
                    defaultValue={file.content}
                    uploadImage={uploadImage}
                    onChange={(valueGetter) => {
                        persistentValueGetter = valueGetter;
                    }}
                />
            </EditorHolder>
        </EditorContainer>
    );
};

export { EditorArea, getEditorValue };