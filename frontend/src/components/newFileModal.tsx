import { Modal, Button, Form } from 'react-bootstrap';
import { useState, FC } from 'react';
import { newFile } from './editorArea/hooks/useFullFile';
import { useAppStateContext } from '../context/appState';
import { FileNames } from 'hexapod-common';

const NewFileModal: FC = () => {
    const [show, setShow] = useState(false);
    const [name, setName] = useState<string>('');
    const [load, setLoad] = useState(false);
    const { document, setDocument } = useAppStateContext();

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    if (!document.type) {
        return <></>
    }

    const sendNewFile = async () => {
        setLoad(true);
        console.log('creating', document.type, name)
        const filename = await newFile(document.type!, name);
        setLoad(false);
        handleClose();
        setDocument({...document, filename})
    }

    const fileType = FileNames[document.type];

    return (
        <>
            <Button variant="primary" onClick={handleShow}>
                New {fileType}
            </Button>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>New {fileType}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3">
                            <Form.Label>{fileType} name</Form.Label>
                            <Form.Control onChange={(e) => setName(e.target.value)} value={name} type="text" placeholder={`Enter ${fileType} name`} />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose} disabled={load}>
                        Cancel
                    </Button>
                    <Button variant="primary" onClick={sendNewFile} disabled={load}>
                        Make {fileType}
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export { NewFileModal };