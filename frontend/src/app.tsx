import { Header } from './components/header';
import { Wrapper, ContentWrapper } from './components/wrapper';
import { LeftPane } from './components/sidePane';
import { EditorArea } from './components/editorArea';
import { Preloader } from './components/preloader';
import { useAppStateContext, AppStates } from './context/appState';

const renderContent = (appState: AppStates) => {
  if (appState === AppStates.mainScreen) {
    return (
      <>
        <Header />
        <ContentWrapper>
          <LeftPane />
          <EditorArea />
        </ContentWrapper>
      </>
    )
  } else if (appState === AppStates.preloading) {
    return (
      <Preloader />
    )
  }
}


const App = () => {
  const { appState } = useAppStateContext();

  return (
    <Wrapper>
      {renderContent(appState)}
    </Wrapper>
  );
}

export default App;
