import ReactDOM from 'react-dom/client';
import './index.css';
import App from './app';
import { AppStateContextWrapper } from './context/appState';


const root = ReactDOM.createRoot(document.getElementById('root') || document.body);
root.render(
  <AppStateContextWrapper>
    <App />
  </AppStateContextWrapper>
);
