import { createContext, useContext, useState, ReactNode, FC } from 'react';
import { FileTypes, noOp } from 'hexapod-common';

export enum AppStates {
    preloading,
    mainScreen
}

export type DocumentInfo = {
    type: FileTypes | null,
    filename: string | null,
}

type TAppStateContext = {
    appState: AppStates,
    setAppState: (nextState: AppStates) => void,
    document: DocumentInfo,
    setDocument: (nextDocument: DocumentInfo) => void,
}

const DefaultAppStateContext: TAppStateContext = {
    appState: AppStates.preloading,
    setAppState: noOp,
    document: {type: FileTypes.posts, filename: null},
    setDocument: noOp,
}

const AppStateContext = createContext<TAppStateContext>(DefaultAppStateContext);

export const useAppStateContext = () => useContext(AppStateContext);

type AppStateWrapperProps = {
    children: ReactNode
}

export const AppStateContextWrapper: FC<AppStateWrapperProps> = ({children}) => {
    const [appState, setAppState] = useState<AppStates>(DefaultAppStateContext.appState);
    const [document, setDocument] = useState<DocumentInfo>(DefaultAppStateContext.document);
    return (
        <AppStateContext.Provider value={{
            appState,
            setAppState,
            document,
            setDocument
        }}>
            {children}
        </AppStateContext.Provider>
    )
}