import getWrapper from '../../scripts/hexoWrapper';
import { Router, Application } from 'express';
import { Middleware } from '../../types';

const start = () => {
    const router = Router();

    router.post('/start', async (req, res, next) => {
        const hexoWrapper = getWrapper();
        if (hexoWrapper.running) {
            return res.json({error: 'Hexo already running'})
        }
        try {
            await hexoWrapper.start();

        } catch(e) {
            return res.json({error: `Hexo failed to start: ${e}`})
        }
        return res.json({ok: true})
    })

    return router;
}

const stop = () => {
    const router = Router();

    router.post('/stop', async (req, res, next) => {
        const hexoWrapper = getWrapper();
        if (!hexoWrapper.running) {
            return res.json({error: 'Hexo already stopped'})
        }
        try {
            await hexoWrapper.stop();
        } catch(e) {
            return res.json({error: `Hexo failed to stop: ${e}`})
        }
        if (!hexoWrapper.running) {
            return res.json({ok: true})
        }
    })

    return router;
}

const status = () => {
    const router = Router();
    router.post('/status', (req, res, next) => {
        return res.json({
            ok: true,
            running: true
        });
    })

    return router;
}

const generate = () => {
    const router = Router();
    const hexoWrapper = getWrapper();
    router.post('/generate', async (req, res, next) => {
        try {
            await hexoWrapper.generate();
            return res.json({ok: true})
        } catch(e) {
            return res.json({error: e.message})
        }
    });

    return router;
}

const test = () => {
    const router = Router();
    const hexoWrapper = getWrapper();
    router.post('/test', async (req, res, next) => {
        try {
            await hexoWrapper.test();
            return res.json({ok: true})
        } catch(e) {
            return res.json({error: e.message})
        }
    });

    return router;
}

export const checkToken: Middleware = (req, res, next) => {
    if (req.get('token') !== 'colour-sound-oblivion') {
        return res.json({error: 'Forbidden'});
    }
    next();
};

const useControlRoutes = (app: Application) => {
    app.use('/hc/control', checkToken, [
        start(), 
        stop(), 
        status(),
        generate(),
        test(),
    ]);
};

export default useControlRoutes;