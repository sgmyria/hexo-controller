import listPostsController from './list';
import newPostController from './new';
import express from 'express';
import { checkToken } from '../control';

const useWritingPostsRoutes = (app: express.Application) => {
    app.use('/hc/writing', checkToken, [
        listPostsController(),
        newPostController(),
    ]);
};

export default useWritingPostsRoutes;