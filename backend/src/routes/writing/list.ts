import { Router } from 'express';
import {
    parseFiles,
    getFileDetails,
    getFileContent,
} from '../../scripts/utils';
import { Middleware } from '../../types';
import { FileTypes } from 'hexapod-common';

const listAll: Middleware = async (req, res) => {
    try {
        const files = await parseFiles(req.params.type as FileTypes);
        res.json({ok: true, files});
    } catch(e) {
        res.json({error: e})
    }
}

const fileDetails: Middleware = async (req, res) => {
    try {
        const {files} = req.body;
        if (!files || !files.length) {
            return res.json({error: 'No files to fetch details for'})
        }
        const details: any = {}
        for( const file of files) {
            try {
                const fileDetails = await getFileDetails(req.params.type as FileTypes, file);
                details[file] = fileDetails;
            } catch(e) {
                console.log('err', e.message)
                return res.json({error: e})
            }              
        }
        res.json({ok: true, details})
    } catch(e) {
        res.json({error: e})
    }
}

const fileContent: Middleware = async (req, res) => {
    try {
        const {file} = req.body;
        const fileContent = await getFileContent(req.params.type as FileTypes, file);
        res.json({ok: true, fileContent})
    } catch(e) {
        res.json({error: e})
    }
}

const listPostsController = () => {
    const router = Router();
    router.post('/listAll/:type', listAll);
    router.post('/fileDetails/:type', fileDetails);
    router.post('/fileContent/:type', fileContent);
    return router;
}

export default listPostsController;