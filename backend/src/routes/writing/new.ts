import { Router } from 'express';
import {
    writeFile,
    deleteFile as delFile,
    getFileDetails,
} from '../../scripts/utils';
import getWrapper from '../../scripts/hexoWrapper';
import { Middleware } from '../../types';
import { FileTypes } from 'hexapod-common';

const newFile: Middleware = async (req, res) => {
    try {
        const {name} = req.body;
        const fileType = req.params.type as FileTypes;
        const hexoWrapper = getWrapper();
        const filename = await hexoWrapper.newFile(fileType, name);
        if (filename) {
            const fileDetails = await getFileDetails(fileType, filename);
            return res.json({ok: true, fileDetails, filename});
        }
        res.json({error: 'Failed to make a new file: wrapper returned blank filename'})
    } catch(e) {
        res.json({error: e})
    }
}

const updateFile: Middleware = async (req, res, next) => {
    try {
        const {name, details, content} = req.body;
        const fileType = req.params.type as FileTypes;
        if (fileType && name && details && content) {
            await writeFile(fileType, name, details, content);
            return res.json({ok: true});
        } else {
            res.json({error: 'Missing either a type, name, details, or content'})
        }
    } catch(e) {
        res.json({error: e.message});
    }

}

const deleteFile: Middleware = async (req, res, next) => {
    try {
        const {name} = req.body;
        const fileType = req.params.type as FileTypes;
        if (fileType && name) {
            await delFile(fileType, name);
            return res.json({ok: true});
        } else {
            res.json({error: 'Missing either a type or name'})
        }
    } catch(e) {
        res.json({error: e.message});
    } 
}

const newFileController = () => {
    const router = Router();
    router.post('/new/:type', newFile);
    router.post('/update/:type', updateFile)
    router.post('/delete/:type', deleteFile)
    return router;
}

export default newFileController;