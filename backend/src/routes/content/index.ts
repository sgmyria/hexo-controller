import { Router, Application } from 'express';
import { genId } from 'hexapod-common';
import { uploadS3File, downloadS3File } from '../../scripts/aws';
import fileUpload from 'express-fileupload';
import { checkToken } from '../control';

const upload = () => {
    const router = Router();

    router.post('/upload', checkToken, fileUpload(), async (req, res, next) => {
        try {
            const file = req.files['file'] as fileUpload.UploadedFile;
            const { mimetype, name } = file;
            const split = name.split('.');
            const extension = split[split.length - 1];
            const metadata = {
                mimetype
            }
            if (!file) {
                return res.json({error: 'No file'});
            }
            const id = genId();
            const filename = `${id}.${extension}`
            await uploadS3File(filename, file.data, metadata)
            return res.json({ok: true, location: `https://caterjunes.com/hc/content/view/${filename}`});
        } catch(e) {
            return res.json({error: `Upload failed: ${e.message}`});
        }
    })

    return router;
}

const view = () => {
    const router = Router();
    router.get('/view/:path', async (req, res, next) => {
        const path = req.params.path;
        const data = await downloadS3File(path);
        const { mimetype } = data.Metadata;
        res.writeHead(200, {
          'Content-Type': mimetype || 'image/jpg',
          'Content-Length': data.ContentLength
        });
        return res.end(data.Body); 
    })
    return router;
}


const useControlRoutes = (app: Application) => {
    app.use('/hc/content', [
        upload(),
        view(),
    ]);
};

export default useControlRoutes;