import fs, { promises } from 'fs';
import readline from 'readline';
import { FileDirectories, FileTypes } from 'hexapod-common';
import { resolve } from 'path';
const { readdir } = promises;

async function getFiles(dir: string): Promise<string[]>{
    const dirents = await readdir(dir, { withFileTypes: true });
    const files = await Promise.all(dirents.map((dirent) => {
        const res = resolve(dir, dirent.name);
        return dirent.isDirectory() ? getFiles(res) : res;
    }));
    return Array.prototype.concat(...files);
}
const HEXO_DIRECTORY = process.env.APP_ENV === 'production' ? '/home/ubuntu/hexo' : `D:\\Users\\Admin\\hexo-blog`;

const processLineByLine = async (file: string, cb: (line: string) => boolean) => {
    const fileStream = fs.createReadStream(file);

    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });

    for await (const line of rl) {
        const result = cb(line);
        if (!result) {
            break;
        }
    }
    return Promise.resolve();
}

const parseFiles = async (type: FileTypes) => {
    const dir = `${HEXO_DIRECTORY}${FileDirectories[type]}`
    let didPrintOnce = false;
    try {
        const files = await getFiles(dir);
        return Promise.resolve(files.map(file => {
            const fdir = FileDirectories[type];
            const res = file.substring(file.indexOf(fdir) + fdir.length + 1);

            if (!didPrintOnce) {
                // console.log('processing', type)
                // console.log(dir)
                // console.log(file)
                // console.log(res)
                didPrintOnce = true;
            }
            return res
        }));
    } catch(e) {
        return Promise.reject(new Error(e))
    }
}

const encodeFileDetails = (details: any) => {
    let result = '';
    for (const prop of Object.keys(details)) {
        result = `${result}${prop}: ${details[prop]}\n`;
    }
    return `---\n${result}---\n`
}

const getFileDetails = (type: FileTypes, file: string) => {
    const dir = `${HEXO_DIRECTORY}${FileDirectories[type]}`
    return new Promise((resolve, reject) => {
        let foundFirstLimit = false;
        const result: any = {};
        processLineByLine(`${dir}/${file}`, (line) => {
            if (!foundFirstLimit && line !== '---') {
                reject('Cannot get file details - invalid header format - no first delimitator');
                return false;
            }
            if (line === '---') {
                if (!foundFirstLimit) {
                    foundFirstLimit = true;
                    return true;
                } else {
                    resolve(result);
                    return false;
                }
            }
            if (foundFirstLimit) {
                if (!line || !line.length) {
                    reject('Cannot get file details - invalid header format - empty line');
                    return false;
                }
                const [propName, propValue] = line.split(': ')
                if (!propName) {
                    reject('Cannot get file details - nameless property')
                    return false;
                }
                const trimmedProp = propName.trim();
                result[trimmedProp] = '';
                if (propValue) {
                    result[trimmedProp] = propValue.trim();
                }
            }
            return true;
        });
    });
}

const getFileContent = (type: FileTypes, file: string) => {
    const dir = `${HEXO_DIRECTORY}${FileDirectories[type]}`
    return new Promise(async (resolve, reject) => {
        let foundFirstLimit = false;
        let foundSecondLimit = false;
        let result = '';
        await processLineByLine(`${dir}/${file}`, (line) => {
            if (!foundFirstLimit && line !== '---') {
                reject('Cannot get file content - invalid header format - no first delimitator');
                return false;
            }
            if (line === '---') {
                if (!foundFirstLimit) {
                    foundFirstLimit = true;
                    return true;
                } else {
                    foundSecondLimit = true;
                    return true;
                }
            }
            if (foundSecondLimit) {
                result = `${result}\n${line}`
            }
            return true;
        });
        resolve(result);
    });
}

const writeFile = (type: FileTypes, file: string, details: any, content: string) => {
    return new Promise<void>((resolve, reject) => {
        const dir = `${HEXO_DIRECTORY}${FileDirectories[type]}`
        console.log('write', type, file)
        fs.writeFile(`${dir}/${file}`, `${encodeFileDetails(details)}${content}`, (err) => {
            if (err) {
                return reject(err)
            }
            resolve();
        });
    });
}

const deleteFile = (type: FileTypes, file: string) => {
    return new Promise<void>((resolve, reject) => {
        const dir = `${HEXO_DIRECTORY}${FileDirectories[type]}`
        console.log('delete', type, file)
        fs.unlink(`${dir}/${file}`, (err) => {
            if (err) {
                return reject(err)
            }
            resolve();
        });
    });
}

export {
    deleteFile,
    writeFile,
    parseFiles,
    getFileDetails,
    getFileContent,
    encodeFileDetails,
}