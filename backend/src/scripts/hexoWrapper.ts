import { exec, ChildProcess } from 'child_process';
import { FileTypes, FileDirectories } from 'hexapod-common';

const opt = {
    cwd: '/home/ubuntu/hexo',
};

const shellString = 'hexo server --port 3000';
const createdString = 'Created: ';

const HexoFileTypes: Record<FileTypes, string> = {
    [FileTypes.posts]: 'post',
    [FileTypes.pages]: 'page',
    [FileTypes.drafts]: 'draft',
}

class HexoWrapper {

    running: boolean
    _process: ChildProcess | null

    constructor() {
        this.running = false;
        this._process = null;
    }

    _clean() {
        this.running = false;
        this._process = null;
    }

    start() {
        return new Promise<void>((resolve, reject) => {
            this._process = exec(shellString, opt, (error, stdout, stderr) => {
                if (error) {
                    reject(new Error(error.message));
                }
            
                if (stderr) {
                    // console.log(`process stderr: ${stderr}`);
                }
            
                // console.log(`process stdout:\n${stdout}`);
            });
            this._process.on('close', this._clean);
            this._process.on('exit', this._clean);
            this._process.on('error', reject);
            this._process.on('spawn', () => {
                this.running = true;
                resolve();
            })
        })
    }

    stop() {
        return new Promise<void>((resolve, reject) => {
            if (this.running) {
                const killString = `killall hexo`;
                const kill = exec(killString, (error, stdout, stderr)=>{
                    if (error) {
                        reject(new Error(error.message))
                    }
                
                    if (stderr) {
                        // console.log(`kill stderr: ${stderr}`);
                    }
                
                    // console.log(`kill stdout:\n${stdout}`);
                })
                kill.on('spawn', resolve)
                kill.on('close', resolve);
                kill.on('exit', resolve);
                kill.on('error', reject);
                this._clean();
            }
        });
    }

    test() {
        return new Promise<void>((resolve, reject) => {
            const genString = 'ls';
            const testcmd = exec(genString, opt, (error, stdout, stderr) => {
                if (error) {
                    return reject(new Error(error.message))
                }
                if (stderr) {
                    // console.log(`test stderr: ${stderr}`);
                }
                if (stdout){
                    // console.log(`test stdout: ${stdout}`)
                }
            });

            testcmd.on('close', () => {
                // console.log('test closed, resolving ')
                resolve()
            });

            testcmd.on('error', (e) => {
                // console.log('test onError', e)
                reject('Error launching test function')
            });
        });
    }

    generate() {
        return new Promise<void>((resolve, reject) => {
            const genString = 'yarn build';
            const generateCmd = exec(genString, opt, (error, stdout, stderr) => {
                if (error) {
                    return reject(new Error(error.message))
                }
                if (stderr) {
                    console.log(`generate stderr: ${stderr}`);
                }
                if (stdout){
                    console.log(`generate stdout: ${stdout}`)
                }
            });

            generateCmd.on('close', () => {
                // console.log('generate closed, resolving ')
                resolve()
            });

            generateCmd.on('error', (e) => {
                // console.log('generate onError', e)
                reject('Error launching hexo generate function')
            });
        });
    }

    newFile(filetype: FileTypes, name: string) {
        return new Promise<string>((resolve, reject) => {
            // console.log('creating new', filetype, name)
            const newString = `hexo new ${HexoFileTypes[filetype]} "${name.replace(/"/g, '\\"')}"`;
            // console.log('with command line:', newString);
            let filename = '';
            const newFile = exec(newString, opt, (error, stdout, stderr)=>{
                if (error) {
                    // console.log(`newfile error: ${error.message}`);
                }
            
                if (stderr) {
                    // console.log(`newfile stderr: ${stderr}`);
                }

                if (stdout) {
                    const linesOut = stdout.split('\n');
                    for (const line of linesOut) {
                        if (!line.includes(createdString)) {
                            continue;
                        } else {
                            const [_, fullFilename] = line.split(createdString);
                            const [__, relativeFilename] = fullFilename.split(FileDirectories[filetype])
                            filename = relativeFilename.substring(1);
                        }
                    }
                }
            
                // console.log(`newfile stdout:\n${stdout}`);
            })

            newFile.on('close', () => {
                // console.log('newfile closed, resolving filename', filename)
                resolve(filename)
            });

            newFile.on('error', (e) => {
                // console.log('newfile onError', e)
                reject('Error launching hexo newFile function')
            });
        });
    }

}

const hw = new HexoWrapper();

const getWrapper = () => {
    return hw;
}

export default getWrapper;