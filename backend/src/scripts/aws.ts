import AWS from 'aws-sdk';

AWS.config.update({
    accessKeyId: 'AKIAULEFHBZI5BMFIX5R',
    secretAccessKey: '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'
});

const BUCKET = 'caterjunes-blog-bucket';

const s3 = new AWS.S3();

const uploadS3File = async (path: string, item: Buffer, Metadata = {}) => {
    return new Promise<AWS.S3.ManagedUpload.SendData>((resolve, reject) => {
        s3.upload({
            Bucket: BUCKET,
            Key: path,
            Body: item,
            Metadata
        }, (err, data) => {
            if (err) reject(err)
            else resolve(data)
        })
    })
}

const downloadS3File = async (path: string) => {
    return new Promise<AWS.S3.GetObjectOutput>((resolve, reject) => {
        s3.getObject({ 
            Bucket: BUCKET, 
            Key: path 
        }, (err, data) => {
            if (err) reject(err)
            else resolve(data)
        })
    })
}

export {
    uploadS3File,
    downloadS3File,
}