import createError from 'http-errors';
import express, {Request, Response, NextFunction} from 'express';
import logger from 'morgan';
import bodyParser from 'body-parser';
import useControlRoutes from './routes/control';
import useWritingPostsRoutes from './routes/writing';
import useContentRoutes from './routes/content';
import cors from 'cors';

// setup
const app = express();
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(cors());


// logging
app.use(logger('dev'));

// Routes
useControlRoutes(app);
useWritingPostsRoutes(app);
useContentRoutes(app);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err: any, req: Request, res: Response, next: NextFunction) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
});

export default app;
